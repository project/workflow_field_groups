Workflow Field Groups Module


INTRODUCTION
=============

The Workflow Field Groups module allows you to control which field groups may be accessed depending on the current workflow state. Control can be specified by role. Currently this only applies to form displays and not view displays. Different settings can also be set per form display mode.


REQUIREMENTS
============

Field Group module
Workflow module


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See: https://www.drupal.org/documentation/install/modules-themes/modules-8


MAINTAINERS
===========

Current Maintainers:
* Michael Welford - https://www.drupal.org/u/mikejw

This project has been sponsored by:
* The University of Adelaide - https://www.drupal.org/university-of-adelaide
  Visit: http://www.adelaide.edu.au
